﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceHelper : MonoBehaviour {

    public void onLoadClick()
    {
        GameObject instance = Instantiate(Resources.Load("Cube", typeof(GameObject))) as GameObject;
        instance.transform.position = new Vector3(10, 10, 10);
    }
}
