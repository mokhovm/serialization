﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// подключенные сериализаторы
/// </summary>
public enum SaveMode
{
    PREFS,
    TEXT,
    XML,
    JSON
}


/// <summary>
/// объект для сериализации
/// </summary>
public class MyObject
{
    public string name;
    public float health;
    public bool isVisible;

    public override string ToString()
    {
        return $"name = {name}, health = {health}, isVisible = {isVisible}";
    }
}

public class SerializeHelper : MonoBehaviour
{
    private MyObject obj;

    void Start()
    {
        obj = new MyObject
        {
            name = "test",
            health = 100,
            isVisible = true
        };
    }

    public void LoadState()
    {
        var dd = FindObjectOfType<Dropdown>();
        doLoad((SaveMode)dd.value);
    }

    public void SaveState()
    {
        var dd = FindObjectOfType<Dropdown>();
        doSave((SaveMode)dd.value);
    }

    protected void doSave(SaveMode mode)
    {
        System.Type sType;
        Debug.Log("try to save " + mode);
        switch (mode)
        {
            case SaveMode.PREFS:
                sType = typeof(PrefSerializator);
                break;
            case SaveMode.TEXT:
                sType = typeof(TextSerializator);
                break;
            case SaveMode.XML:
                sType = typeof(XMLSerializator);
                break;
            case SaveMode.JSON:
                sType = typeof(JsonSerializator);
                break;
            default: throw new Exception("invalid serializer type");
        }
        SerializeManager m = new SerializeManager(sType);
        m.setOptions(Application.dataPath);
        m.save(obj);
        Debug.Log("done...");
    }

    protected void doLoad(SaveMode mode)
    {
        System.Type sType;
        MyObject newObj;
        Debug.Log("try to load " + mode);
        switch (mode)
        {
            case SaveMode.PREFS:
                sType = typeof(PrefSerializator);
                break;
            case SaveMode.TEXT:
                sType = typeof(TextSerializator);
                break;
            case SaveMode.XML:
                sType = typeof(XMLSerializator);
                break;
            case SaveMode.JSON:
                sType = typeof(JsonSerializator);
                break;
            default: throw new Exception("invalid serializer type");
        }
        SerializeManager m = new SerializeManager(sType);
        m.setOptions(Application.dataPath);
        newObj = m.load();
        Debug.Log(newObj.ToString());
    }

}




