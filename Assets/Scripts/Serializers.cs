﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

/// <summary>
/// Интерфейс сериализации
/// </summary>
public interface ISaver
{
    MyObject load();
    void save(MyObject obj);
    void setOptions(string aPath);
}

/// <summary>
/// базовый класс всех сериализаторов
/// </summary>
public abstract class CustomSerializator : ISaver
{
    protected const string DEF_EXT = ".cfg";

    protected const string FILE_NAME = "MyObject";

    protected const string OBJ_NAME = "name";
    protected const string OBJ_HEALTH = "health";
    protected const string OBJ_VISIBLE = "isVisible";



    protected string _path;
    protected string _ext = DEF_EXT;

    public abstract MyObject load();

    public abstract void save(MyObject obj);

    public void setOptions(string aPath)
    {
        _path = Path.Combine(aPath, FILE_NAME);
        _path = Path.ChangeExtension(_path, _ext);

    }

}

/// <summary>
/// Хранит даные в реестре 
/// </summary>
public class PrefSerializator : CustomSerializator
{
    public override MyObject load()
    {
        var res = new MyObject();
        var key = OBJ_NAME;
        if (PlayerPrefs.HasKey(key))
        {
            res.name = PlayerPrefs.GetString(key);
        }
        key = OBJ_HEALTH;
        if (PlayerPrefs.HasKey(key))
        {
            res.health = PlayerPrefs.GetFloat(key);
        }
        key = OBJ_VISIBLE;
        if (PlayerPrefs.HasKey(key))
        {
            res.isVisible = PlayerPrefs.GetString(key).TryBool();
        }
        return res;
    }

    public override void save(MyObject obj)
    {
        PlayerPrefs.SetString(OBJ_NAME, obj.name);
        PlayerPrefs.SetFloat(OBJ_HEALTH, obj.health);
        PlayerPrefs.SetString(OBJ_VISIBLE, obj.isVisible.ToString());
        PlayerPrefs.Save();
    }

}

/// <summary>
/// хранит данные в тектовом виде
/// </summary>
public class TextSerializator : CustomSerializator
{

    public override MyObject load()
    {
        var reult = new MyObject();
        if (!File.Exists(_path)) return reult;
        using (StreamReader streamReader = new StreamReader(_path))
        {
            while (!streamReader.EndOfStream)
            {
                reult.name = streamReader.ReadLine();
                reult.health = System.Convert.ToSingle(streamReader.ReadLine());
                reult.isVisible = streamReader.ReadLine().TryBool();
            }
        }
        return reult;
    }

    public override void save(MyObject obj)
    {
        using (var sw = new StreamWriter(_path))
        {
            sw.WriteLine(obj.name);
            sw.WriteLine(obj.health);
            sw.WriteLine(obj.isVisible);
        }

    }
}

public static class Extensions
{
    public static bool TryBool(this string self)
    {
        bool result;
        return Boolean.TryParse(self, out result) && result;
    }
}



/// <summary>
/// Хранит данные в XML
/// </summary>
public class XMLSerializator : CustomSerializator
{
    protected new const string DEF_EXT = ".xml";

    public XMLSerializator() : base()
    {
        _ext = DEF_EXT;
    }

    public override MyObject load()
    {
        var reult = new MyObject();
        if (!File.Exists(_path)) return reult;
        using (XmlTextReader reader = new XmlTextReader(_path))
        {
            while (reader.Read())
            {
                if (reader.IsStartElement(OBJ_NAME))
                {
                    reult.name = reader.GetAttribute("value");
                }
                if (reader.IsStartElement(OBJ_HEALTH))
                {
                    reult.health =
                        System.Convert.ToSingle(reader.GetAttribute("value"));
                }
                if (reader.IsStartElement(OBJ_VISIBLE))
                {
                    reult.isVisible = reader.GetAttribute("value").TryBool();
                }
            }
        }
        return reult;

    }

    public override void save(MyObject obj)
    {
        var xmlDoc = new XmlDocument();
        XmlNode rootNode = xmlDoc.CreateElement("MyObject");
        xmlDoc.AppendChild(rootNode);
        var element = xmlDoc.CreateElement(OBJ_NAME);
        element.SetAttribute("value", obj.name);
        rootNode.AppendChild(element);
        element = xmlDoc.CreateElement(OBJ_HEALTH);
        element.SetAttribute("value", obj.health.ToString());
        rootNode.AppendChild(element);
        element = xmlDoc.CreateElement(OBJ_VISIBLE);
        element.SetAttribute("value", obj.isVisible.ToString());
        rootNode.AppendChild(element);
        XmlNode userNode = xmlDoc.CreateElement("Info");
        var attribute = xmlDoc.CreateAttribute("Unity");
        attribute.Value = Application.unityVersion;
        userNode.Attributes?.Append(attribute);
        userNode.InnerText = "System Language: " +
                             Application.systemLanguage;
        rootNode.AppendChild(userNode);
        xmlDoc.Save(_path);
    }

}

/// <summary>
/// Хранит данные в JSON
/// </summary>
public class JsonSerializator : CustomSerializator
{
    protected new const string DEF_EXT = ".json";

    public JsonSerializator() : base()
    {
        _ext = DEF_EXT;
    }

    override public MyObject load()
    {
        var str = File.ReadAllText(_path);
        return JsonUtility.FromJson<MyObject>(str);
    }

    override public void save(MyObject obj)
    {
        var str = JsonUtility.ToJson(obj);
        File.WriteAllText(_path, str);

    }

}

/// <summary>
/// Управляет сериализацией и десериализацией данных
/// </summary>
class SerializeManager
{
    private ISaver m_serializer;

    public SerializeManager(Type aType)
    {
        m_serializer = Activator.CreateInstance(aType) as ISaver;
        //if (aType is CustomSerializator)
        //    m_serializer = Activator.CreateInstance(aType) as ISaver;
        //else
        //    throw new Exception("Invalid serializator type");
    }

    public void save(MyObject obj)
    {
        m_serializer.save(obj);
    }

    public MyObject load()
    {
        return m_serializer.load();
    }

    public void setOptions(string path)
    {
        m_serializer.setOptions(path);
    }

}
